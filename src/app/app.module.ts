import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './components/home/home.component';
import { MaterialModule } from './modules/material/material.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ListSchoolYearComponent } from './components/school-year/list-school-year/list-school-year.component';
import { NgxHalClientModule } from '@lagoshny/ngx-hal-client';
import { ExternalConfigurationService } from './services/data/external-configuration-service.service';
import { SchoolYearService } from './services/data/school-year-service.service';
import { DetailSchoolYearComponent } from './components/detail-school-year/detail-school-year.component';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
    ListSchoolYearComponent,
    DetailSchoolYearComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    MaterialModule,
    NgxHalClientModule.forRoot(),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://localhost:8080/'],
        sendAccessToken: true
      }
    }),
  ],
  providers: [
    {provide: 'ExternalConfigurationService', useClass: ExternalConfigurationService},
    { provide: OAuthStorage, useValue: sessionStorage },
    SchoolYearService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
