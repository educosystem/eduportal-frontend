import { Injectable, Injector } from '@angular/core';
import { RestService } from '@lagoshny/ngx-hal-client';
import { SchoolYear } from '../../models/school-year';

@Injectable()
export class SchoolYearService extends RestService<SchoolYear>{
  constructor(injector: Injector) {
    super(SchoolYear, 'schoolYears', injector);
  }
}
