import { Injectable } from '@angular/core';
import { ExternalConfigurationHandlerInterface, ExternalConfiguration } from '@lagoshny/ngx-hal-client';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ExternalConfigurationService implements ExternalConfigurationHandlerInterface {

  constructor(private http: HttpClient) { }

  deserialize() {
  }

  serialize() {
  }

  getProxyUri(): string {
    return '';
  }

  getRootUri(): string {
    return 'http://localhost:8080/';
  }

  getHttp(): import('@angular/common/http').HttpClient {
    return this.http;
  }

  getExternalConfiguration(): ExternalConfiguration {
    return null;
  }

  setExternalConfiguration(externalConfiguration: ExternalConfiguration) {
  }
}
