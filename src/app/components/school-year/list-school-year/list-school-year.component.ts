import { Component, OnInit } from '@angular/core';
import { SchoolYearService } from 'src/app/services/data/school-year-service.service';
import { SchoolYear } from 'src/app/models/school-year';
import { MatListItem } from '@angular/material/list';
import { PageEvent } from '@angular/material/paginator';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-list-school-year',
  templateUrl: './list-school-year.component.html',
  styleUrls: ['./list-school-year.component.scss']
})
export class ListSchoolYearComponent implements OnInit {
  schoolYears: SchoolYear[];
  size: number;
  private pageSize: number;
  private pageIndex: number;
  filterName: string;
  filterError: boolean;

  constructor(private schoolYearService: SchoolYearService, private oauthService: OAuthService) {
    console.log(oauthService.getAccessToken());
  }

  ngOnInit(): void {
    this.pageSize = 10;
    this.pageIndex = 0;
    this.filterName = '';
    this.filterError = false;
    this.getAllSchoolYearsAvailable();
  }

  /**
   * Try to get all school year available.
   */
  getAllSchoolYearsAvailable() {
    const options = {
      params: [
        { key: 'page', value: this.pageIndex },
        { key: 'size', value: this.pageSize },
        { key: 'sort', value: 'name' },
        { key: 'name', value: this.filterName }]
    };
    this.schoolYearService.getAll(options)
      .subscribe((schoolYears: SchoolYear[]) => {
        this.schoolYears = schoolYears;
        this.size = this.schoolYearService.totalElement();
      });
  }

  /**
   * Handle the changeVisibility action.
   * @param event to prevent the click view and navigation to detail schoolYear.
   * @param i number of the item list.
   */
  toogleVisibility(event: Event, list: MatListItem, i: number) {
    event.preventDefault();
    event.stopImmediatePropagation();
    if (this.schoolYears[i].visible) {
      this.schoolYears[i].patchRelation('invisible', null).subscribe(newSchoolYear => {
        this.schoolYears[i] = newSchoolYear;
      });
    } else {
      this.schoolYears[i].patchRelation('visible', null).subscribe(newSchoolYear => {
        this.schoolYears[i] = newSchoolYear;
      });
    }
  }

  /**
   * Handle the paginator events.
   * @param event of paginator.
   */
  changePage(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getAllSchoolYearsAvailable();
  }

  /**
   * Handle the search box actions.
   * @param text to search.
   */
  onKey(text: string) {
    if (text.length === 0 || text.length >= 3) {
      this.filterName = text;
      this.filterError = false;
    } else {
      this.filterError = true;
    }
    if (!this.filterError) {
      this.getAllSchoolYearsAvailable();
    }
  }
}
