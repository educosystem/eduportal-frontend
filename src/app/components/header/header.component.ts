import { Component, OnInit, Output, EventEmitter, Inject, HostListener } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { authConfig } from 'src/app/auth.config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() toogleNavbar: EventEmitter<any> = new EventEmitter();
  scrollInTop = true;
  authenticated: boolean;

  constructor(private oauthService: OAuthService) {

  }

  ngOnInit(): void { }

  /**
   * Handle the scroll event to show a float toolbar or not.
   */
  @HostListener('window:scroll', ['$event'])
  doSomethingOnWindowsScroll() {
    this.scrollInTop = window.scrollY === 0;
  }

  /**
   * Handle the menu cliked item.
   */
  clicked() {
    this.toogleNavbar.emit();
  }

  /**
   * Handle the log in button action and start authentication.
   */
  public login() {
    this.oauthService.configure(authConfig);
    this.oauthService.initImplicitFlow();
  }

  /**
   * Handle the log out button action and forgot authentication.
   */
  public logout() {
    this.oauthService.configure(authConfig);
    this.oauthService.logOut();
  }
}
