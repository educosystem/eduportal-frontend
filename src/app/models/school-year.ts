import { Resource } from '@lagoshny/ngx-hal-client';

export class SchoolYear extends Resource {
  cod: string;
  name: string;
  visible: boolean;
  matriculationKey: string;
}
