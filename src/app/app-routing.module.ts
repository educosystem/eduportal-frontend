import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ListSchoolYearComponent } from './components/school-year/list-school-year/list-school-year.component';
import { DetailSchoolYearComponent } from './components/detail-school-year/detail-school-year.component';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'schoolYear', component: ListSchoolYearComponent},
  { path: 'schoolYear/:id', component: DetailSchoolYearComponent},
  { path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
