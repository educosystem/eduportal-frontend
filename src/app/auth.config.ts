import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  issuer: 'http://localhost:8080/oauth/',
  loginUrl: 'http://localhost:8080/oauth/authorize',
  logoutUrl: 'http://localhost:8080/exit',
  redirectUri: window.location.origin + '/',
  postLogoutRedirectUri: window.location.origin + '/',
  clientId: 'web',
  scope: 'ALL',
  requireHttps: false,
  disablePKCE: true,
  oidc: false,
};
